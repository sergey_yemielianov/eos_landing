window.$ = window.jQuery = require('jquery');

$(document).ready(function() {
    generateCalendarDays();

    $('.animated-link').click(function (event) {
        event.preventDefault();
        const link = $(this).attr('href');
        $('body,html').animate({
            scrollTop: $(link).offset().top
        }, 800);
    });

    $('#next_to_select_the_date').click(function (event){
        $('#form_step1').submit();
    });

    $('#back_to_enter_details').click(function (event) {
        $('.slider').css('left','0');
        $('.contact-form-block-content').toggleClass('--hidden',false);
        $('.date-selector-part').toggleClass('--hidden',true);
    });

    $('#form_step1').submit(function (event) {
        event.preventDefault();

        let name = $('#name');
        let number = $('#number');
        let email = $('#email');
        let username = $('#username');
        let message = $('#message');

        let validName = validateName(name.val());
        name.toggleClass('input-error', !validName);
        let validNumber = validateNumber(number.val());
        number.toggleClass('input-error', !validNumber);
        let validEmail = validateEmail(email.val());
        email.toggleClass('input-error', !validEmail);
        let validUsername = validateUsername(username.val());
        username.toggleClass('input-error', !validUsername);
        let validMessage = validateMessage(message.val());
        message.toggleClass('input-error', !validMessage);

        if(validName === true && validEmail === true && validNumber === true && validUsername === true && validMessage === true) {
            $('.slider').css('left','-100%');
            $('.date-selector-part').toggleClass('--hidden',false);
            $('.contact-form-block-content').toggleClass('--hidden',true);
        }
    });

    $('#form_step1 input').keyup(function(){
        $(this).removeClass('input-error');
        $('#contact_submit_step1').toggleClass('active-button',validateForm1());
    });

    $('#form_step2').submit(function (event) {
        event.preventDefault();

        let comment = $('#comment');
        let date = $('[name="date"]:checked');
        let time = $('[name="time"]:checked');

        let validComment = validateComment(comment.val());
        comment.toggleClass('input-error', !validComment);
        let validDate = validateDate(date.length);
        $('.date-selector').find('label').toggleClass('input-error', !validDate);
        let validTime = validateTime(time.length);
        $('.time-selector').find('label').toggleClass('input-error', !validTime);

        if(validComment === true && validDate === true && validTime === true) {
            $('.slider').css('transition','none').css('left','-200%');
            $('.thanks-message').toggleClass('--hidden',false);
            $('.date-selector-part').css('transition','none').toggleClass('--hidden',true);
        }
    });

    $('#form_step2 input').change(function(){
        $(this).removeClass('input-error');
        $('#contact_submit_step2').toggleClass('active-button',validateForm2());
    });

    $('#form_step2 textarea').keyup(function(){
        $(this).removeClass('input-error');
        $('#contact_submit_step2').toggleClass('active-button',validateForm2());
    });

    function validateForm1(){
        let name = $('#name');
        let number = $('#number');
        let email = $('#email');
        let username = $('#username');
        let message = $('#message');

        let validName = validateName(name.val());
        let validNumber = validateNumber(number.val());
        let validEmail = validateEmail(email.val());
        let validUsername = validateUsername(username.val());
        let validMessage = validateMessage(message.val());

        return validName && validNumber && validEmail && validUsername && validMessage;
    }

    function validateForm2(){
        let comment = $('#comment');
        let date = $('[name="date"]:checked');
        let time = $('[name="time"]:checked');

        let validComment = validateComment(comment.val());
        let validDate = validateDate(date.length);
        let validTime = validateTime(time.length);

        return validComment && validDate && validTime;
    }

    function validateName(value) {
        return value !== '';
    }

    function validateNumber(value) {

        return value !== '';
    }

    function validateEmail(value) {
        return value !== '';
    }

    function validateUsername(value) {
        return value !== '';
    }

    function validateMessage(value) {
        return value !== '';
    }

    function validateComment(value) {
        return value !== '';
    }

    function validateDate(value) {
        return value > 0;
    }

    function validateTime(value) {
        return value > 0;
    }

    function generateCalendarDays() {
        let date = new Date();
        const dateSelector = $('.date-selector');
        for (let i = 0; i < 16; i++) {
            date.setDate(date.getDate() + 1);
            const fullDate = `${date.getDate()}-${date.getMonth()+1}-${date.getFullYear()}`;
            let dayTemplate = `
                <div class="selector" data-month="${date.getMonth()}">
                    <input type="radio" id="${fullDate}"
                           name="date" value="${fullDate}"/>
                    <label for="${fullDate}">
                    <span class="select-day">
                        ${getDayName(date.getDay())}
                    </span>
                    <span class="select-number">
                        ${date.getDate()}
                    </span>
                    </label>
                </div>`;
            dateSelector.append(dayTemplate);
        }
    }

    function getDayName(day) {
        const days = [
            'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'
        ];
        return days[day];
    }

    function getMonthName(month) {
        const months = [
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ];
        return months[month];
    }

    let page = 0;
    setDaysPage();

    $('.arrow-left').click(function() {
        if (page>0) {
            page--;
            setDaysPage();
        }
    });

    $('.arrow-right').click(function() {
        if (page<3) {
            page++;
            setDaysPage();
        }
    });

    function setDaysPage() {
        let dateSelector = $('.date-selector');
        dateSelector.css('left', (-100*page)+'%');
        $('.arrow-left').toggleClass('--inactive', page===0);
        $('.arrow-right').toggleClass('--inactive', page===3);
        let month = $(dateSelector.find('.selector')[page*4]).data('month');
        $('.month-name').text(getMonthName(month));
    }

    $('#open_mobile_menu').click(function () {
        $('.header-items-wrapper').addClass('--active');
        $('.background-menu-mask').addClass('--active');
    });

    $('#close_mobile_menu').click(function () {
        $('.header-items-wrapper').removeClass('--active');
        $('.background-menu-mask').removeClass('--active');
    });

});

